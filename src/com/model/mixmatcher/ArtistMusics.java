package com.model.mixmatcher;

import java.util.ArrayList;

public class ArtistMusics {
	
	private String artist;
	private ArrayList<String> musics = new ArrayList<String>();
	
	public ArtistMusics(String artist, ArrayList<String> musics) {
		super();
		this.artist = artist;
		this.musics = musics;
	}
	
	public ArtistMusics() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ArrayList<String> getMusics() {
		return musics;
	}

	public void setMusics(ArrayList<String> musics) {
		this.musics = musics;
	}
	
	public void addMusic (String musicTitle){
		musics.add(musicTitle);
	}
	
	
}