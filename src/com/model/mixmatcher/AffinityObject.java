package com.model.mixmatcher;

import java.math.BigDecimal;

public class AffinityObject {
	
	private String artist;
	private Float match;
	
	
	public AffinityObject() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AffinityObject(String artist, Float match) {
		super();
		this.artist = artist;
		this.match = match;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public Float getMatch() {
		return match;
	}
	public void setMatch(Float match) {
		this.match = match;
	}
	
}