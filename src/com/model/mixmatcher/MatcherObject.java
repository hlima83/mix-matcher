package com.model.mixmatcher;

public class MatcherObject {
	
	private String artist;
	private int count;
	
	public MatcherObject(String artist, int count) {
		super();
		this.artist = artist;
		this.count = count;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public void incCount(){
		this.count++;
	}

}