package com.model.mixmatcher;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class RelatedArtistHelper extends SQLiteOpenHelper {

    //private static final String DB_PATH = "/data/data/com.android.sqlite/databases/";
    private static final String DB_PATH = "/databases/matcher/";
    private static final String DB_NAME = "matchers.db";

	public static final String TABLE_ARTIST = "artists";
	public static final String COLUMN_ARTIST_ID = "_id";
	public static final String COLUMN_ARTIST_NAME = "name";
	
	public static final String TABLE_ARTISTS_RELATIONS = "artists_relations";
	public static final String COLUMN_ARTISTS_RELATIONS_ID = "_id";
	public static final String COLUMN_ARTISTS_RELATIONS_ARTIST1 = "artist1_id";
	public static final String COLUMN_ARTISTS_RELATIONS_ARTIST2 = "artist2_name";
	public static final String COLUMN_ARTISTS_RELATIONS_AFFINITY = "affinity";
	
	//public static final String CREATE_ARTISTS = "CREATE TABLE artists (id INTEGER PRIMARY KEY, name TEXT)";
	public static final String DROP_ARTISTS = "DROP TABLE IF EXISTS artists";
	public static final String DROP_ARTISTS_RELATIONS = "DROP TABLE IF EXISTS artists_relations";
	public static final String CREATE_ARTISTS = "CREATE TABLE IF NOT EXISTS artists (_id INTEGER PRIMARY KEY, name TEXT)";
	//public static final String CREATE_ARTISTS_RELATIONS = "CREATE TABLE artists_relations (id INTEGER PRIMARY KEY, artist1_id NUMERIC, artist2_name TEXT, affinity NUMERIC)";
	public static final String CREATE_ARTISTS_RELATIONS = "CREATE TABLE IF NOT EXISTS artists_relations (_id INTEGER PRIMARY KEY, artist1_id NUMERIC, artist2_name TEXT, affinity NUMERIC)";
	
    
    /**
     * @param context
     * @param name
     * @param factory
     * @param version	
    */
    public RelatedArtistHelper(Context context, String name,
    	    CursorFactory factory, int version) {
    	super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    	System.out.println("creating database");

    	db.execSQL(DROP_ARTISTS);
    	db.execSQL(DROP_ARTISTS_RELATIONS);
    	db.execSQL(CREATE_ARTISTS);
    	db.execSQL(CREATE_ARTISTS_RELATIONS);
    	//db.close();
    	
//        query += "ALTER TABLE artists_relations ADD FOREIGN KEY (artist1_id) REFERENCES artists(artist_id)";
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	
    	System.out.println("upgrading database");

    	db.execSQL(CREATE_ARTISTS);
    	db.execSQL(CREATE_ARTISTS_RELATIONS);
    	
    }

    /**
     * Check if the database already exist to avoid
     * re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
//    public boolean checkDataBase(){
//
//        SQLiteDatabase checkDB = null;
//
//        try{
//            String myPath = DB_PATH + DB_NAME;
//            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
//
//
//        }catch(SQLiteException e){
//            //database does't exist yet.
//        }
//
//        if(checkDB != null){
//            checkDB.close();
//        }
//
//        return checkDB != null ? true : false;
//    }
}