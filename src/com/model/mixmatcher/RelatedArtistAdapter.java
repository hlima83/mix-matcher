package com.model.mixmatcher;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class RelatedArtistAdapter {
	
	private static final String DB_NAME = "mixer.db";
	public static final int MYDATABASE_VERSION = 1;

    private final String SELECT_ARTISTS = "SELECT * FROM artists";

	 
	private RelatedArtistHelper relatedArtistHelper;
	private SQLiteDatabase relatedArtistDatabase;
	 
	private Context context;
	 
	 public RelatedArtistAdapter(Context context){
	  this.context = context;
	 }
	 
	 public SQLiteDatabase openToRead() throws android.database.SQLException {
		  relatedArtistHelper = new RelatedArtistHelper(this.context, DB_NAME, null, MYDATABASE_VERSION);
		  relatedArtistDatabase = relatedArtistHelper.getReadableDatabase();
		  //MYDATABASE_VERSION++;
		  return relatedArtistDatabase; 
	}
		 
	 public RelatedArtistAdapter openToWrite() throws android.database.SQLException {
		  relatedArtistHelper = new RelatedArtistHelper(this.context, DB_NAME, null, MYDATABASE_VERSION);
		  relatedArtistDatabase = relatedArtistHelper.getWritableDatabase();
		  //MYDATABASE_VERSION++;
		  //relatedArtistHelper.close();
		  return this; 
	}

    public void deleteArtists(){
        relatedArtistDatabase.delete(RelatedArtistHelper.TABLE_ARTISTS_RELATIONS, null, null);
        relatedArtistDatabase.delete(RelatedArtistHelper.TABLE_ARTIST, null, null);
    }
	
		public void insertRelatedArtists(ArrayList<AffinitiesList> relatedArtists){
		
		Integer artist_id = 1;
		Integer artist_relations_id = 1;
		
		try{
			
		for(AffinitiesList affList : relatedArtists){
			
			ContentValues artist_val = new ContentValues();
			String artist_name = affList.getLibArtist();
			System.out.println("inserting " + artist_name);
			artist_val.put(RelatedArtistHelper.COLUMN_ARTIST_ID, artist_id);
			artist_val.put(RelatedArtistHelper.COLUMN_ARTIST_NAME, artist_name);
			relatedArtistDatabase.insert(RelatedArtistHelper.TABLE_ARTIST, null, artist_val);
			
			for (AffinityObject affinity : affList.getArtistAffinities()){
				
				ContentValues artist_relations_vals = new ContentValues();
				artist_relations_vals.put(RelatedArtistHelper.COLUMN_ARTISTS_RELATIONS_ID, artist_relations_id);
				artist_relations_vals.put(RelatedArtistHelper.COLUMN_ARTISTS_RELATIONS_ARTIST1, artist_id);
				artist_relations_vals.put(RelatedArtistHelper.COLUMN_ARTISTS_RELATIONS_ARTIST2, affinity.getArtist());
				artist_relations_vals.put(RelatedArtistHelper.COLUMN_ARTISTS_RELATIONS_AFFINITY, affinity.getMatch());
				relatedArtistDatabase.insert(RelatedArtistHelper.TABLE_ARTISTS_RELATIONS, null, artist_relations_vals);
				
				artist_relations_id++;
				
			}
			
			artist_id++;
		}
		}
		
		catch(Exception e){
			System.out.println("DATABASE PROBLEMS:" + e);
		}
		
	}
	 
	 public long insertionTest(){
		  
		  ContentValues artist_val = new ContentValues();
		  artist_val.put(RelatedArtistHelper.COLUMN_ARTIST_ID, 1);
		  artist_val.put(RelatedArtistHelper.COLUMN_ARTIST_NAME, "Blur");
		  //relatedArtistDatabase.insert(RelatedArtistHelper.TABLE_ARTIST, null, artist_val);
		  return relatedArtistDatabase.insert(RelatedArtistHelper.TABLE_ARTIST, null, artist_val);
	 }
	 
	 public String getAllAffinities(){
		 String[] columns = new String[]{RelatedArtistHelper.COLUMN_ARTIST_NAME};
		 Cursor cursor = relatedArtistDatabase.query(RelatedArtistHelper.TABLE_ARTIST, columns, 
		    null, null, null, null, null);
		  String result = "";
		  
		  int index_CONTENT = cursor.getColumnIndex(RelatedArtistHelper.COLUMN_ARTIST_NAME);
		  for(cursor.moveToFirst(); !(cursor.isAfterLast()); cursor.moveToNext()){
		   result = result + cursor.getString(index_CONTENT) + "\n";
		  }
		  
		  return result;
	 }

     public ArrayList<String> getAllArtists(){

         ArrayList<String> artistsArr = new ArrayList<String>();
         Cursor artistsCursor = relatedArtistDatabase.rawQuery(SELECT_ARTISTS, null);
         artistsCursor.moveToFirst();

         while(!artistsCursor.isAfterLast()){
            artistsArr.add(artistsCursor.getString(1));
             artistsCursor.moveToNext();
         }

         return artistsArr;
     }
		 
	 public void close(){
		  relatedArtistHelper.close();
	}
	 
}