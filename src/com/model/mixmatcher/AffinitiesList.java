package com.model.mixmatcher;

import java.util.ArrayList;

public class AffinitiesList {

	private String libArtist;
	private ArrayList<AffinityObject> artistAffinities;
	
	public AffinitiesList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AffinitiesList(String libArtist,
			ArrayList<AffinityObject> artistAffinities) {
		super();
		this.libArtist = libArtist;
		this.artistAffinities = artistAffinities;
	}

	public String getLibArtist() {
		return libArtist;
	}

	public void setLibArtist(String libArtist) {
		this.libArtist = libArtist;
	}

	public ArrayList<AffinityObject> getArtistAffinities() {
		return artistAffinities;
	}

	public void setArtistAffinities(ArrayList<AffinityObject> artistAffinities) {
		this.artistAffinities = artistAffinities;
	}
	
}
