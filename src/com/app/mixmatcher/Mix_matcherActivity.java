package com.app.mixmatcher;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;
import com.model.mixmatcher.RelatedArtistAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;


public class Mix_matcherActivity extends Activity {
	
	private final String SELECT_ARTISTS = "SELECT * FROM artists";
	private final String SELECT_ARTISTS_RELATIONS = "SELECT * FROM artists_relations where artist1_id = ?";
	//private final String SELECT_ARTISTS_RELATIONS = "SELECT * FROM artists_relations";

    private ArrayList<String> localArtists;
    private String[] remoteArtists;
    private Float affinityLevel;
    public ArrayList<String> similarArtists = new ArrayList<String>();
	
	//private RelatedArtistDataSource;
	final int REQUEST_ENABLE_BT = 0;
	final int REQUEST_DISCOVERY = 1;
	final int BLUETOOTH_DURATION = 300;
	
	private BluetoothDevicesReceiver mReceiver;
	BluetoothSocket serverSocket;
	BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	
	private ArrayAdapter<BluetoothDevice> devicesList;
	public static ListView layoutList;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("entered application");
        
        //set the layout
        setContentView(R.layout.mix_switcher);
        layoutList = (ListView) findViewById(R.id.mates_list);

        //get local bands
        RelatedArtistAdapter relatedArtistAdapter = new RelatedArtistAdapter(this);
        relatedArtistAdapter.openToRead();
        localArtists = relatedArtistAdapter.getAllArtists();
        relatedArtistAdapter.close();

        //set click event to list elements
          layoutList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

              public void onItemClick(AdapterView<?> arg0, View view,
                                      int index, long arg3) {
                  AsyncTask<Integer, Void, Void> connectTask =
                          new AsyncTask<Integer, Void, Void>() {
                              @Override
                              protected Void doInBackground(Integer... params) {
                                  try {

                                      Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
                                      //if there isn't a connection
                                      //if(pairedDevices.size() == 0){
                                      BluetoothDevice device = BluetoothHelper.foundDevices.get(params[0]);
                                      BluetoothSocket socket = device.createRfcommSocketToServiceRecord(BluetoothConfigurations.BLUETOOTH_UUID);
                                      socket.connect();
                                      String receivedMsg = BluetoothHelper.receiveMessage(socket);
                                      socket.close();
                                      System.out.println("Received msg: " + receivedMsg);

                                      Gson gson = new Gson();
                                      remoteArtists = gson.fromJson(receivedMsg, String[].class);
                                      List<String> remoteList = Arrays.asList(remoteArtists);
                                      unregisterReceiver(mReceiver);
                                      calculateAffinity(remoteList);
                                      runOnUiThread(new Runnable() {
                                          public void run() {
                                              setupDetail();
                                          }
                                      });
                                    
                                      socket.close();
                                      //}
                                  } catch (IOException e) {
                                      System.out.println(e);
                                  }
                                  return null;
                              }

                              @Override
                              protected void onPostExecute(Void result) {

                              }
                          };
                  connectTask.execute(index);
              }
          });

//        Float affinityLevel = calculateAffinity(deviceList);
//
    	//start bluetooth connections
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        //start listening for remote devices
        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE), REQUEST_DISCOVERY);
    }

    private void setupDetail(){

        TextView affinityTxt = (TextView) findViewById(R.id.textAff);
        affinityTxt.setText("Affinity: " + affinityLevel.toString());

        ListView similarList = (ListView) findViewById(R.id.list_similar);
        ArrayAdapter<String> similars = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, similarArtists);
        similarList.setAdapter(similars);

        ViewSwitcher switcher = (ViewSwitcher) findViewById(R.id.mix_switch);
        switcher.showNext();


    }

    public void startDiscovery(View view){

        System.out.println("Discovering...");

        mReceiver = new BluetoothDevicesReceiver(new Handler());
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy

        if (!mBluetoothAdapter.isDiscovering()) {
            BluetoothHelper.foundDevices.clear();
            mBluetoothAdapter.startDiscovery();
          }
    }

//    @Override
//    protected void onPause(){
//        unregisterReceiver(mReceiver);
//    }
//
//    @Override
//    protected void onResume(){
//        unregisterReceiver(mReceiver);
//    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
    	
    	if(requestCode == REQUEST_DISCOVERY){
    		boolean isDiscoverable = resultCode > 0;
    		
    		if (isDiscoverable) {
    		
    		try{
    			
    			
    			final BluetoothServerSocket btServer = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(BluetoothConfigurations.BLUETOOH_SERVER, BluetoothConfigurations.BLUETOOTH_UUID);

    			AsyncTask<Integer, Void, BluetoothSocket> acceptThread =
    		            new AsyncTask<Integer, Void, BluetoothSocket>() {

    		            @Override
    		            protected BluetoothSocket doInBackground(Integer... params) {
    		              try {
    		                serverSocket = btServer.accept(params[0]*1000);
                            if(serverSocket != null){
                                Gson gson = new Gson();
                                String blueMsg = gson.toJson(localArtists);
                                BluetoothHelper.sendMessage(serverSocket, blueMsg);
                                //serverSocket.close();
                            }
    		                return serverSocket;
    		              } catch (IOException e) {
    		            	  System.out.println("Error while listening: " + e);
    		              }
    		              return null;
    		            }

//    		            @Override
//    		            protected void onPostExecute(BluetoothSocket socket) {
//    		              if (socket != null)
//    		                BluetoothHelper.sendMessage(socket, "I'm listening!");
//    		            }
    		          };

    		          acceptThread.execute(resultCode);
    		          
    		} catch(IOException e){
    			System.out.println("error listening for devices...");
    		}
    	}
    }
}

    private void calculateAffinity(List<String> deviceList){
    	
    	Float affLevel = Float.valueOf(0);
    	
    	RelatedArtistAdapter relatedArtistAdapter = new RelatedArtistAdapter(this);
        SQLiteDatabase affinitiesDatabase = relatedArtistAdapter.openToRead();
        
    	Cursor artistsCursor = affinitiesDatabase.rawQuery(SELECT_ARTISTS, null);
    	Integer affDiv = artistsCursor.getCount();

        if(affDiv > 0){
        artistsCursor.moveToFirst();
    	while(artistsCursor.isAfterLast() == false){
    		
    		if(deviceList.contains(artistsCursor.getString(1))){
    			affLevel += 1/affDiv;
    		}
    		else{
    		
    			Float affParcel = Float.valueOf(0);
    			
    			for(String deviceArtist : deviceList){
    				
    				Cursor relationsCursor = affinitiesDatabase.rawQuery(SELECT_ARTISTS_RELATIONS, new String []{String.valueOf(artistsCursor.getInt(0))});

                    if(relationsCursor.getCount() > 0){
                    relationsCursor.moveToFirst();
    				while(relationsCursor.isAfterLast() == false){
                        String relationBand = relationsCursor.getString(2);
    					if(deviceArtist.equals(relationBand)){
                            if(!similarArtists.contains(relationBand)){
                                similarArtists.add(relationBand);
                            }
    						affParcel += relationsCursor.getFloat(3);
    						break;
    					}
                        relationsCursor.moveToNext();
    				}
                    }
    			}
    			
    		affLevel += affParcel/affDiv;
    		
    		}
    		
    		artistsCursor.moveToNext();
    	}
        }

        relatedArtistAdapter.close();
    	affinityLevel = affLevel;
  
    	
    	}
    
    
    
//    @Override
//    public void onDestroy(){
//    	System.out.println("destroying");
//    	unregisterReceiver(mReceiver);
//    }
// 
}