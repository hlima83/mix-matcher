package com.app.mixmatcher;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class BluetoothHelper {
	
	public static ArrayList<BluetoothDevice> foundDevices = new ArrayList<BluetoothDevice>();
	//public static ListView layoutList;
	
	private static final int BUFFER_SIZE = 1024;
	
	public static void sendMessage(BluetoothSocket socket, String message){
		
		OutputStream outMsg;
		
		try{
			outMsg = socket.getOutputStream();
			byte[] byteArray = (message + " ").getBytes();
			byteArray[byteArray.length -1] = 0;
			
			outMsg.write(byteArray);
		} catch (IOException e){
			System.out.println("Exception while sending bluetooth message " + e);
		}
		
	}
	
	public static String receiveMessage(BluetoothSocket socket){
		
		String result = new String();
		byte[] buffer = new byte[BUFFER_SIZE];
		
		try{
			InputStream inMsg = socket.getInputStream();
			int bytesRead = -1;
			
			while(true){
				bytesRead = inMsg.read(buffer);
				
				if(bytesRead != -1){
					while ((bytesRead == BUFFER_SIZE) && (buffer[BUFFER_SIZE - 1] != 0)){
						result += new String(buffer, 0, bytesRead);
						bytesRead = inMsg.read(buffer);
					}
					
					result += new String(buffer, 0, bytesRead - 1);
					
				}
			}
			
			} catch (IOException e){
				System.out.println("Exception while receiving bluetooth message: " + e);
			}
		
		
		
		return result;
	}
}
