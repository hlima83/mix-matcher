package com.app.mixmatcher;

import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.model.mixmatcher.AffinitiesList;
import com.model.mixmatcher.AffinityObject;
import com.model.mixmatcher.ArtistMusics;
import com.utils.mixmatcher.MixMatcherUtils;


public class AffinitiesHelper {
	
	private static final String LASTFM_KEY = "9d43e80f5f98590444ba3f61a405f4da";
	//private static final String USER = "KicK07";
	
	public static ArrayList<AffinitiesList> buildAffinities(ArrayList<ArtistMusics> bandsList){
		
		// Create a new HttpClient and Post Header
		//HttpClient httpClient = MixMatcherUtils.getThreadSafeClient();
		String httpReq = "http://ws.audioscrobbler.com/2.0/?method=artist.getSimilar&api_key=" + LASTFM_KEY + "&artist=";
		
		ArrayList<AffinitiesList> bandsAffinities = new ArrayList<AffinitiesList>();
					
		for(ArtistMusics b : bandsList){				
			
			String band = b.getArtist();
			AffinitiesList listAff = new AffinitiesList();
			listAff.setLibArtist(band);
			
			String bandFormatted = band.replaceAll(" ", "%20");
			System.out.println(bandFormatted);
					
			try {
			    HttpPost httppost = new HttpPost(httpReq + bandFormatted);
			   
			    HttpClient httpclient = MixMatcherUtils.getThreadSafeClient();
			    // Execute HTTP Post Request
			    HttpResponse response = httpclient.execute(httppost);
			    
			    ArrayList<AffinityObject> artistAffinities = parseResponse(response);
			    listAff.setArtistAffinities(artistAffinities);
			    bandsAffinities.add(listAff);
			    
			} catch (ClientProtocolException e) {
				System.out.println("request unsuccessfull:" + e);
			} catch (IOException e) {
				System.out.println("request unsuccessfull:" + e);
			} catch (java.lang.IllegalArgumentException e){
				System.out.println("bad artist name:" + e);
			}

		}	
		
		return bandsAffinities;
		
	}
	
	public static ArrayList<AffinityObject> parseResponse(HttpResponse response){
		
		ArrayList<AffinityObject> matchList = new ArrayList<AffinityObject>();
		
		try{
			
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document xmlResponse = db.parse(new DataInputStream(response.getEntity().getContent()));
			
			//System.out.println(EntityUtils.toString(response.getEntity()));
			NodeList artistsList = xmlResponse.getElementsByTagName("artist");
			System.out.println("size=" + artistsList.getLength());
			
			 for (int i = 0; i < artistsList.getLength(); i++) {
				 
				 Element e = (Element)artistsList.item(i);
				 
				 Element artistElem = (Element)e.getElementsByTagName("name").item(0);
				 String artistName = artistElem.getChildNodes().item(0).getNodeValue();
				 
				 Element matchElem = (Element)e.getElementsByTagName("match").item(0);
				 Float artistMatch = new Float(matchElem.getChildNodes().item(0).getNodeValue());
				 
				 AffinityObject affinityElem = new AffinityObject(artistName, artistMatch);
				 matchList.add(affinityElem);
				 
			 }
			 
		} catch (IOException e){
			return null;
		} catch (ParserConfigurationException e){
			return null;
		} 
		catch (SAXException e){
			return null;
		}
		
		return matchList;
			
		
		
		

		
	}
	
}