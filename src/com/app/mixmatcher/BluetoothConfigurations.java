package com.app.mixmatcher;

import java.util.UUID;

public class BluetoothConfigurations {
	
	public static final UUID BLUETOOTH_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	public static final String BLUETOOH_SERVER = "matcherserver";
	
}
