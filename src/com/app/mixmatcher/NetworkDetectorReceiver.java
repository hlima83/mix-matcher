package com.app.mixmatcher;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

public class NetworkDetectorReceiver extends BroadcastReceiver{

    public static boolean connected = false;
	@Override
	public void onReceive(final Context context, final Intent intent) {

		if(intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
		    NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
		    Intent serviceIntent = new Intent();
		    if(networkInfo.isConnected()) {
                if(connected){
                    return;
                }
                else{
                    connected = true;
                }
	        	System.out.println("NETWORK IS ON!");
	        	
	        	ActivityManager activityManager = (ActivityManager) context.getSystemService("com.app.mixmatcher.AffinityLibraryUpdater");
	        	
	        	//check if service is already running
	        	if(activityManager == null){
	        		serviceIntent.setAction("com.app.mixmatcher.AffinityLibraryUpdater");
	        		context.startService(serviceIntent);
	        		context.stopService(serviceIntent);
	        	}
	        	else{
	        		System.out.println("Service already running");
                    return;
	        	}
	        	
		    }
		    else{
		    	//TODO: turn off service
		    	context.stopService(serviceIntent);
		    	System.out.println("NETWORK IS OFF!");
                connected = false;
		    }
		}

		else if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
		    NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
		    if(networkInfo.isConnected() && !connected) {
                if(connected){
                    return;
                }
                else{
                    connected = true;
                }
		    	System.out.println("NETWORK IS ON!");
		    	
		    	ActivityManager activityManager = (ActivityManager) context.getSystemService("com.app.mixmatcher.AffinityLibraryUpdater");
	        	
	        	//check if service is already running
	        	if(activityManager == null){
	        		Intent serviceIntent = new Intent();
	        		serviceIntent.setAction("com.app.mixmatcher.AffinityLibraryUpdater");
	        		context.startService(serviceIntent);
	        		context.stopService(serviceIntent);
	        	}
                else{
                    System.out.println("Service already running");
                    return;
                }
		    }
		    else{
		    	//TODO: turn off service
                connected = false;
		        System.out.println("NETWORK IS OFF!");
		    }
		}
		
	}
	
}