package com.app.mixmatcher;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.ArrayAdapter;

public class BluetoothDevicesReceiver extends BroadcastReceiver{

    private Handler handler;
    public ArrayAdapter<BluetoothDevice> devicesList;

    public BluetoothDevicesReceiver(Handler handler){
        this.handler = handler;
    }



	@Override
	public void onReceive(final Context context, final Intent intent) {
		
		System.out.println("receiving something...");
		String action = intent.getAction();
		if(BluetoothDevice.ACTION_FOUND.equals(action)){
			System.out.println("DEVICE DISCOVERED!!!");
			BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if(!BluetoothHelper.foundDevices.contains(device)){
            BluetoothHelper.foundDevices.add(device);

            handler.post(new Runnable(){
                @Override
                public void run(){
                    devicesList = new ArrayAdapter<BluetoothDevice>(context, android.R.layout.simple_list_item_1, BluetoothHelper.foundDevices);
                    Mix_matcherActivity.layoutList.setAdapter(devicesList);
                }
            }
            );

			//
			
//			//get own UUID
//			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//			System.out.println(mBluetoothAdapter.getAddress());
//			
//			try{
//				BluetoothSocket clientSocket = device.createRfcommSocketToServiceRecord(BluetoothConfigurations.BLUETOOTH_UUID);
//				clientSocket.connect();
//			} catch (IOException e){
//				System.out.println("cant communicate");
//				//Toast.makeText(context, "Error connecting device " + device.getName(), Toast.LENGTH_SHORT).show();
//			}
		}
		
		//String remoteDeviceName = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
		
	

	}
    }
}