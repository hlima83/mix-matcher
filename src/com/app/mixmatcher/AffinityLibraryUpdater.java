package com.app.mixmatcher;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;
import com.model.mixmatcher.AffinitiesList;
import com.model.mixmatcher.ArtistMusics;
import com.model.mixmatcher.RelatedArtistAdapter;
import org.farng.mp3.MP3File;

import java.io.File;
import java.util.ArrayList;

public class AffinityLibraryUpdater extends Service {

	private ArrayList<String> mp3List = new ArrayList<String>();
	private ArrayList<ArtistMusics> bandsList = new ArrayList<ArtistMusics>();
	private ArrayList<AffinitiesList> bandAffinities = new ArrayList<AffinitiesList>();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		System.out.println("Loading library...!");
		LoadLibraryTask loadLibTask = new LoadLibraryTask();
		loadLibTask.execute();
		
//		Toast userNotification = Toast.makeText(this, "DONE!!! ",
//				Toast.LENGTH_LONG);
//		userNotification.show();
		// this.stopSelf();

		return START_STICKY;
	}


	@Override
	public IBinder onBind(Intent intent) {

		return null;

	}

	@Override
	public void onCreate() {
		super.onCreate();

		Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// System.out.println("destroyed!!!");
		Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();

	}

	@Override
	public void onStart(Intent intent, int startId) {

		// System.out.println("service started");		
		super.onStart(intent, startId);

		Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

	}

	public void stopService() {
		// System.out.println( "!!!!!");
		this.stopSelf();
	}

	public Context getContext() {
		return this;
	}

	// Load library task
	public class LoadLibraryTask extends
			AsyncTask<String, Void, Integer> {
		
		protected Integer doInBackground(String... strings) {

			System.out.println("service entered...");

            if(bandsList.size() > 0){
                return 1;
            }

            File f = Environment.getExternalStorageDirectory();
            findMusic(f);

            f = new File("mnt/ext_card");
            if(f.exists()){
                findMusic(f);
            }

			for (String foo : mp3List) {
				MP3File mp3;
				try {
					mp3 = new MP3File(foo);
 					String id3 = new String();
					String id3Title = new String();
					
					if (mp3.getID3v1Tag() != null){
						id3Title = mp3.getID3v1Tag().getSongTitle();
						id3 = mp3.getID3v1Tag().getArtist();
					}
					else if (mp3.getID3v2Tag() != null){
						id3Title = mp3.getID3v2Tag().getSongTitle();
						id3 = mp3.getID3v2Tag().getLeadArtist()
								.replaceAll("[^\\x00-\\x7F]", "");
					}
					else
						continue;
					
					if(id3 == null || id3.length() == 0 || id3Title == null || id3Title.length() == 0)
						continue;
					
					//testar aqui
					ArrayList<String> listArtists = getArtistsList();
					int bandInd = listArtists.indexOf(id3);

					if (bandInd == -1 && id3.length() > 0) {
						ArtistMusics artist = new ArtistMusics();
						artist.setArtist(id3);
						bandsList.add(artist);
						bandsList.get(bandsList.size()-1).addMusic(id3Title);
					}
					else{
						int musicInd = bandsList.get(bandInd).getMusics().indexOf(id3Title);
						
						if(musicInd == -1){
							bandsList.get(bandInd).addMusic(id3Title);
						}
						
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			return 1;

		}

		private void findMusic(File f) {

			for (File f1 : f.listFiles()) {
				if (f1.isDirectory() && f1.canRead())
					findMusic(f1);
				if (f1.getName().endsWith("mp3"))
					mp3List.add(f1.getAbsolutePath());
			}
		}
		
		public ArrayList<String> getArtistsList(){
			ArrayList<String> artists = new ArrayList<String>();
			
			for(ArtistMusics artist : bandsList){
				artists.add(artist.getArtist());
			}
			
			return artists;
		}

		protected void onPostExecute(Integer result) {	
			
			System.out.println("Load library: " + this.getStatus());
			System.out.println(result + " library finished!");

            if(bandsList.size() > 0){
			    BuildAffinitiesTask buildAffTask = new BuildAffinitiesTask();
			    buildAffTask.execute();
            }
            else{
                stopService();
            }
		} 
	}

	// Load library task
	public class BuildAffinitiesTask extends
			AsyncTask<String, Void, Integer> {

		protected Integer doInBackground(
				String... strings) {

			// build lastfm affinities
			System.out.println("building affinities...");
			
			if(bandAffinities.size() > 0){
				return 1;
			}
			
			bandAffinities = AffinitiesHelper.buildAffinities(bandsList);
			return 1;

		}

		protected void onPostExecute(Integer result) {
			
			System.out.println("affinities built...");
			WriteAffinitiesTask writeAffTask = new WriteAffinitiesTask();
			writeAffTask.execute();
		}
	}

	// Load library task
	public class WriteAffinitiesTask extends
			AsyncTask<String, Void, Integer> {
			
		private RelatedArtistAdapter relatedArtistAdapter;
		
		protected Integer doInBackground(
				String... strings) {
			
			System.out.println("starting database...");
			relatedArtistAdapter = new RelatedArtistAdapter(getContext());
			
			relatedArtistAdapter.openToWrite();
            relatedArtistAdapter.deleteArtists();
			relatedArtistAdapter.insertRelatedArtists(bandAffinities);
	        
			relatedArtistAdapter.close();
			
			//System.out.println("Writing in database...");
			// write affinities on the database
			//RelatedArtistDataSource dataSource = new RelatedArtistDataSource(getContext());
			//dataSource.insertRelatedArtists(bandAffinities);
			
			return 1;
			

		}

		protected void onPostExecute(ArrayList<String> result) {
			
			stopService();

		}
	}
}